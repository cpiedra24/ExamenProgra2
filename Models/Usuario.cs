using System;

namespace examenPro.Models
{
    public class usuario
    {
        public int ID { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string userName { get; set; }
        public string contrasena { get; set; }
    }
}