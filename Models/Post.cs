using System;

namespace examenPro.Models
{
    public class post
    {
        public int ID { get; set; }
        public string titulo { get; set; }
        public DateTime hoy {get; set;}
        public string detalle { get; set; }
        public int UsuarioId { get; set; }
        public virtual usuario usuario { get; set; }
    }
}