using Microsoft.EntityFrameworkCore;

namespace examenPro.Models
{
    public class MvcDataContext : DbContext
    {
        public MvcDataContext(DbContextOptions<MvcDataContext> options)
            : base(options)
        {
        }
        public DbSet<post> post { get; set; }
        public DbSet<usuario> usuario { get; set; }
    }
}