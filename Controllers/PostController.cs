using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using examenPro.Models;
using Microsoft.AspNetCore.Http;

namespace examenPro.Controllers
{
    public class PostController : Controller
    {
        private readonly MvcDataContext _context;

        public PostController(MvcDataContext context)
        {
            _context = context;
        }

        // GET: Post
        public async Task<IActionResult> Index()
        {
            var mvcDataContext = _context.post.OrderByDescending(p => p.hoy);
            return View(await mvcDataContext.ToListAsync());
        }

        // GET: Post/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.post
                .Include(p => p.usuario)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Post/Create
        public IActionResult Create()
        {
            ViewData["UsuarioId"] = new SelectList(_context.usuario, "ID", "userName");
            return View();
        }

        // POST: Post/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,titulo,hoy,detalle,UsuarioId")] post post)
        {
            if (ModelState.IsValid)
            {
                post.hoy=DateTime.Now;
                
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UsuarioId"] = new SelectList(_context.usuario, "ID", "userName", post.UsuarioId);
            return View(post);
        }

        // GET: Post/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.post.SingleOrDefaultAsync(m => m.ID == id);
            if (post == null)
            {
                return NotFound();
            }
            ViewData["UsuarioId"] = new SelectList(_context.usuario, "ID", "userName", post.UsuarioId);
            return View(post);
        }

        // POST: Post/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,titulo,hoy,detalle,UsuarioId")] post post)
        {
            if (id != post.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(post);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!postExists(post.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UsuarioId"] = new SelectList(_context.usuario, "ID", "userName", post.UsuarioId);
            return View(post);
        }

        // GET: Post/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.post
                .Include(p => p.usuario)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Post/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _context.post.SingleOrDefaultAsync(m => m.ID == id);
            _context.post.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool postExists(int id)
        {
            return _context.post.Any(e => e.ID == id);
        }
                public IActionResult Login()
        {
            return View();
        }
 
        [HttpPost]
        public async Task<IActionResult> Login([Bind("userName,contrasena")] usuario usuario)
        {
            var usua = await _context.usuario
               .SingleOrDefaultAsync(u => u.userName == usuario.userName && u.contrasena == usuario.contrasena);

            if (usua != null)
            {
                HttpContext.Session.SetString("userName", usua.userName.ToString());
                ViewBag.nombre = usua.userName;
                return RedirectToAction("Index");
            }

            else
            {
                ModelState.AddModelError("", "El usuario o contra incorrectas");
            }
            return View();
        }
    }
}
